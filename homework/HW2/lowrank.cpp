#include <iostream>

#include <SCvector.h>
#include <SCmatrix.h>

#include <chrono>

// include ...

using namespace std;


using namespace SC;

// use COMPLEX synonymously for complex<double>
typedef complex<double> COMPLEX;

int main()
{
    Matrix<COMPLEX> a(3,2);
    a.SetAll(COMPLEX(2,1));
    a(0,0) = 1; a(1,1) = 3;
    Matrix<COMPLEX> b(4,2);
    b.SetAll(0);
    b(0,0) = COMPLEX(3,2); b(3,0) = COMPLEX(1,2); b(1,1) = 1; b(2,1) = COMPLEX(0,1);

    LowRankMatrix<COMPLEX> M(a, b);

    Vector<COMPLEX> e(4);
    e(0) = 0; e(1) = 1; e(3) = COMPLEX(2,2), e(2) = 2;
    Vector<COMPLEX> f(3);
    f(0) = COMPLEX(1,1); f(1) = 1; f(2) = COMPLEX(2,2);

    // test Apply
    cout << "Test Apply" << endl;
    Vector<COMPLEX> Me;
    Vector<COMPLEX> Me_true(3);
    Me_true[0] = COMPLEX(10,-5);
    Me_true[1] = COMPLEX(17,-4);
    Me_true[2] = COMPLEX(18,-1);

    M.Apply(e, Me);

    cout << "my result   " << Me << endl;
    cout << "true result " << Me_true << endl;

    // test ApplyT
    cout << "Test ApplyT" << endl;

    Vector<COMPLEX> MTf;
    Vector<COMPLEX> MTf_true(4);
    MTf_true[0] = COMPLEX(31,14);
    MTf_true[1] = COMPLEX(6,9);
    MTf_true[2] = COMPLEX(9,-6);
    MTf_true[3] = COMPLEX(21,-2);

    M.ApplyT(f, MTf);

    cout << "my result   " << MTf << endl;
    cout << "true result " << MTf_true << endl;

    // test ApplyH
    cout << "Test ApplyH" << endl;

    Vector<COMPLEX> MHf;
    Vector<COMPLEX> MHf_true(4);
    MHf_true[0] = COMPLEX(23,24);
    MHf_true[1] = COMPLEX(12,3);
    MHf_true[2] = COMPLEX(-3,12);
    MHf_true[3] = COMPLEX(5,20);

    M.ApplyH(f, MHf);

    cout << "my result   " << MHf << endl;
    cout << "true result " << MHf_true << endl;

    // test Get
    cout << "Test Get" << endl;
    Matrix<COMPLEX> MMat(3,4);
    Matrix<COMPLEX> MMat_true(3,4);
    MMat_true(0,0) = COMPLEX(3,-2);
    MMat_true(0,1) = COMPLEX(2,1);
    MMat_true(0,2) = COMPLEX(1,-2);
    MMat_true(0,3) = COMPLEX(1,-2);

    MMat_true(1,0) = COMPLEX(8,-1);
    MMat_true(1,1) = COMPLEX(3,0);
    MMat_true(1,2) = COMPLEX(0,-3);
    MMat_true(1,3) = COMPLEX(4,-3);

    MMat_true(2,0) = COMPLEX(8,-1);
    MMat_true(2,1) = COMPLEX(2,1);
    MMat_true(2,2) = COMPLEX(1,-2);
    MMat_true(2,3) = COMPLEX(4,-3);

    for (int i=0; i<3; i++)
        for (int j=0; j<4; j++)
            MMat(i,j) = M.Get(i,j);

    cout << "my result   " << MMat << endl;
    cout << "true result " << MMat_true << endl;

 


}