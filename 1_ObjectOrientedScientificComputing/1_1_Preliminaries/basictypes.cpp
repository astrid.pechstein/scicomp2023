#include <iostream>

using namespace std;


int main()
{
    // declare different objects a, d, e, b
    int a;
    double d;
    char e;
    bool b;

    // set values accoring to type
    d = 4.5;
    a = d;
    e = 'e';
    b = true;

    printf("as char: %c\n", e);
    printf("as int: %i\n", e);
    printf("as hex: %x\n", e);

    // cout is used to write to console, endl means newline
    cout << "int a  = " << a << endl;
    cout << "double d  = " << d << endl;
    cout << "char e  = " << e << endl;
    cout << "bool b  = " << b << endl;

    // strings are implemented within the standard c++ library (include <iostream>)
    string name;
    name = "ABC";
    cout << name << endl;

    // double d has already been declared, cannot be redeclared within the same scope.
    // however, it is possible to declare int d within its own scope (use {})
    // outside this scope, d is still double and not changed
    {
        int d;
        d = 2;
        cout << "int d = " << d << endl;
    }

    cout << "double d = " << d << endl;
}
