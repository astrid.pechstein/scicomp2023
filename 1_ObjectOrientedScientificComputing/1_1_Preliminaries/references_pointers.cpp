#include <iostream>

using namespace std;

// call by value, does not work
void Increase_cbv(double d)
{
    // print memory address of d inside function
    cout << "CBV: internal address " << &d << endl;
    d++;
    cout << "CBV: internal value of d " << d << endl;
}

// call by reference
void Increase_CBR(double &d)
{
    // print memory address of d inside function
    cout << "CBR: internal address " << &d << endl;
    d++;
    cout << "CBR: internal value of d " << d << endl;
}

// call by reference, pointer argument
void Increase_CBR(double *p)
{
    // print memory address of pointer inside function
    cout << "CBR - ptr: internal address p " << p << endl;
    // increase value
    (*p)++;
    cout << "CBR - ptr: internal value (*p) " << (*p) << endl;
}


int main()
{
    double d = 5.;

    
    cout << "value of d:   " << d << endl;   // print value of d
    cout << "address of d: " << &d << endl;  // print address of d

    cout << "value at address of d: " <<  *(&d) << endl;  // print value of double stored at address of d


    
    cout << "---------------------------------------------" << endl;
    cout << "Call by value" << endl;

    double e = 4.;
    // print memory address of "outside" e
    cout << "CBV: external address " << &e << endl;
    Increase_cbv(e);
    // value of e
    cout << "CBV: after function call: value of e " << e << endl;

    cout << "---------------------------------------------" << endl;
    cout << "Call by reference" << endl;

    // print memory address of "outside" e
    cout << "CBR: external address " << &e << endl;
    Increase_CBR(e);
    // value of d
    cout << "CBR: after function call: value of e " << e << endl;



    cout << "---------------------------------------------" << endl;
    cout << "Pointers" << endl;
    double* ptr = &d;
    cout << "value of pointer " << ptr << endl;
    cout << "de-referencing the pointer gives the value " << *ptr << endl;

    cout << "---------------------------------------------" << endl;
    cout << "Call by reference, using pointer argument" << endl;

    // print memory address of "outside" e
    cout << "CBR: external address " << &e << endl;
    Increase_CBR(&e);
    // value of d
    cout << "CBR: after function call: value of e " << e << endl;

    // default return value of int main()
    return 0;
}

